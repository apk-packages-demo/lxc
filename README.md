## LXC

### Alpine
* [lxc site:pkgs.alpinelinux.org](https://google.com/search?q=lxc+site%3Apkgs.alpinelinux.org)
* [wiki.alpinelinux.org](https://wiki.alpinelinux.org/) > 
  * [***Category:Virtualization***](https://wiki.alpinelinux.org/wiki/Category:Virtualization)
  * [***Howto-lxc-simple***](https://wiki.alpinelinux.org/wiki/Howto-lxc-simple)
  * [***LXC***](https://wiki.alpinelinux.org/wiki/LXC)
  * [***Install Alpine Linux in LXC***](https://wiki.alpinelinux.org/wiki/Install_Alpine_on_LXC)
  * [***Configure Networking***](https://wiki.alpinelinux.org/wiki/Configure_Networking)
* [pkgs.alpinelinux](https://pkgs.alpinelinux.org/packages?name=lxc)
  * [**lxc**](https://pkgs.alpinelinux.org/packages?name=lxc)
  * [**lxc-doc**](https://pkgs.alpinelinux.org/packages?name=lxc-doc)
  * [**lxc-templates**](https://pkgs.alpinelinux.org/packages?name=lxc-templates)
    * `/usr/share/lxc/templates/`

### Documentation by source
* [*Infrastructure for container projects*](https://linuxcontainers.org/) >
  * [***Introduction***](https://linuxcontainers.org/lxc/introduction/)
  * [***Getting started***](https://linuxcontainers.org/lxc/getting-started/)
* [*Ubuntu Server Guide*](https://help.ubuntu.com/lts/serverguide/) >
[*Virtualisation*](https://help.ubuntu.com/lts/serverguide/virtualization.html) > 
[*LXC*](https://help.ubuntu.com/lts/serverguide/lxc.html) >
[*Basic usage*](https://help.ubuntu.com/lts/serverguide/lxc.html#lxc-basic-usage) >
***Nesting*** (LTS 18.04)
(Also [*Is it possible to start LXC container inside LXC container?*](https://serverfault.com/questions/366575/is-it-possible-to-start-lxc-container-inside-lxc-container))
* [*wiki.gentoo.org/wiki/LXC*](https://wiki.gentoo.org/wiki/LXC)
* Man pages
  * 5 ***lxc.conf***
    * https://linuxcontainers.org/fr/lxc/manpages/man5/lxc.conf.5.html
    * https://gitlab.com/alpinelinux-packages-demo/lxc-doc

* [*Image server for LXC and LXD*](https://us.images.linuxcontainers.org/)
* [Index of /images](https://us.images.linuxcontainers.org/images/)

### Documentation by topic
#### LXC container inside LXC container and related configuration files
* Man lxc.conf (for configuration files)
* [*Ubuntu Server Guide*](https://help.ubuntu.com/lts/serverguide/) >
[*Virtualisation*](https://help.ubuntu.com/lts/serverguide/virtualization.html) > 
[*LXC*](https://help.ubuntu.com/lts/serverguide/lxc.html) >
[*Basic usage*](https://help.ubuntu.com/lts/serverguide/lxc.html#lxc-basic-usage) >
***Nesting*** (LTS 18.04)
* [*Is it possible to start LXC container inside LXC container?*](https://serverfault.com/questions/366575/is-it-possible-to-start-lxc-container-inside-lxc-container)
* [*Change lxc container directory*](https://stackoverflow.com/questions/25286546/change-lxc-container-directory)
* [*Where are the LXC container configuration files located?*](https://askubuntu.com/questions/846258/where-are-the-lxc-container-configuration-files-located)

#### LXC UNIX socket
* [lxc unix socket](https://www.google.com/search?q=lxc+unix+socket)
* [lxc unix socket php](https://www.google.com/search?q=lxc+unix+socket+php)
* [*Setting up nginx and PHP-FPM in Docker with Unix Sockets*](https://medium.com/@shrikeh/setting-up-nginx-and-php-fpm-in-docker-with-unix-sockets-6fdfbdc19f91)

### New features
* The download template has been introduced recently, it is not always available!
  * check:
    * `/usr/share/lxc/templates/`
    * `/etc/lxc/templates/`

### See also similar projects
* [hub-docker-com-demo/ubuntu](https://gitlab.com/hub-docker-com-demo/ubuntu)